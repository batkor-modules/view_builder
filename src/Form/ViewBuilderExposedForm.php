<?php

namespace Drupal\view_builder\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ViewBuilderExposedForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'view_builder_exposed_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return $this->getViewBuilderFormElements($form_state->getStorage());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Return form elements from viewBuilderPlugin.
   *
   * @param array $storage
   * @return array
   */
  protected function getViewBuilderFormElements(array $storage){
    $formElements = [];
    /** @var \Drupal\view_builder\ViewBuilderPluginInterface $view_builder*/
    if (isset($storage['view_builder']) && $view_builder = $storage['view_builder']){
      $formElements = $view_builder->exposedFormElements();
    }
    return $formElements;
  }

}
