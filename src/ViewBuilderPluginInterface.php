<?php

namespace Drupal\view_builder;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface ViewBuilderPluginInterface extends PluginInspectionInterface {

  /**
   * Return render array.
   *
   * @return array.
   */
  public function renderer();

  /**
   * Return plugin title or plugin class short name.
   *
   * @return string
   * @throws \ReflectionException
   */
  public function getTitle();

  /**
   * Set route parameters.
   *
   * @param array $params
   * @return $this|\Drupal\view_builder\ViewBuilderPluginInterface
   */
  public function setParameters(array $params);

  /**
   * Return parameter from current route.
   *
   * @param $key
   * @return mixed|null
   */
  public function getParameter(string $key);

  /**
   * Return form render array.
   *
   * @return array|mixed|\Symfony\Component\HttpFoundation\Response
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function renderExposedForm();

  /**
   * Return array form elements for exposed form.
   *
   * Example usage submit element:
   * @code
   * $form['submit'] = [
   *   '#type' => 'submit',
   *    // Prevent from showing up in \Drupal::request()->query.
   *   '#name' => '',
   *   '#value' => $this->t('Apply'),
   *   '#submit' => [[$this, 'exposedFormSubmit']],
   *   '#button_type' => 'primary',
   * ];
   *  @endcode
   * @return array
   */
  public function exposedFormElements();

}
