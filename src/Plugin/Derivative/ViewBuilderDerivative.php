<?php

namespace Drupal\view_builder\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\view_builder\ViewBuilderPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ViewBuilderDerivative extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The base plugin ID.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * This view builder plugin manager service.
   *
   * @var \Drupal\view_builder\ViewBuilderPluginManager
   */
  protected $viewBuilderPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('plugin.manager.view_builder.page')
    );
  }

  /**
   * ViewBuilderBlock constructor.
   *
   * @param $base_plugin_id
   * @param \Drupal\view_builder\ViewBuilderPluginManager $viewBuilderPluginManager
   */
  public function __construct($base_plugin_id, ViewBuilderPluginManager $viewBuilderPluginManager) {
    $this->basePluginId = $base_plugin_id;
    $this->viewBuilderPluginManager = $viewBuilderPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->viewBuilderPluginManager->getDefinitions() as $definition) {
      if (isset($definition['type']) && $definition['type'] === 'block') {
        /** @var \Drupal\view_builder\ViewBuilderPluginInterface $plugin_instance */
        $plugin_instance = $this->viewBuilderPluginManager->createInstance($definition['id']);
        $this->derivatives[$plugin_instance->getPluginId()] = [
            'admin_label' => $plugin_instance->getTitle(),
          ] + $base_plugin_definition;
      }
    }
    return $this->derivatives;
  }
}