<?php

namespace Drupal\view_builder\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\view_builder\ViewBuilderPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a generic Menu block.
 *
 * @Block(
 *   id = "view_builder_block",
 *   admin_label = @Translation("View builder block"),
 *   category = @Translation("View builder"),
 *   deriver = "Drupal\view_builder\Plugin\Derivative\ViewBuilderDerivative",
 * )
 */
class ViewBuilderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * This view builder plugin manager service.
   *
   * @var \Drupal\view_builder\ViewBuilderPluginManager
   */
  protected $plugin_service;

  /**
   * @var \Drupal\view_builder\ViewBuilderPluginInterface
   */
  protected $plugin_instance;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ViewBuilderPluginManager $viewBuilderPluginManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->plugin_service = $viewBuilderPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.view_builder.page')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->getPluginInstance()->renderer();
  }

  protected function getPluginInstance(){
    if ($this->plugin_instance){
      return $this->plugin_instance;
    }
    $this->plugin_instance = $this->plugin_service->createInstance($this->getDerivativeId());
    return $this->plugin_instance;
  }

}
