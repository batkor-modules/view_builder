<?php

namespace Drupal\view_builder\Plugin\ViewBuilder;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\view_builder\ViewBuilderPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ViewBuilderBase extends PluginBase implements ContainerFactoryPluginInterface, ViewBuilderPluginInterface {

  /**
   * The exposed form data.
   *
   * @var array
   */
  protected $exposed_data;

  /**
   * The route parameters.
   *
   * @var array
   */
  protected $parameters;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * Constructs a PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManager $entityTypeManager, FormBuilder $formBuilder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $formBuilder;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    $isset_title = isset($this->pluginDefinition['title']);
    return $isset_title ? $this->pluginDefinition['title'] : (new \ReflectionClass(static::class))->getShortName();
  }

  /**
   * {@inheritdoc}
   */
  public function setParameters(array $params) {
    $this->parameters = $params;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter($key) {
    $isset = isset($this->parameters[$key]);
    return $isset ? $this->parameters[$key] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function renderExposedForm() {
    $form_state = (new FormState())
      ->setStorage([
        'view_builder' => $this,
        'rerender' => TRUE,
      ])
      ->setMethod('get')
      ->setAlwaysProcess()
      ->disableRedirect();
    $form = $this->formBuilder->buildForm('\Drupal\view_builder\Form\ViewBuilderExposedForm', $form_state);
    $form['form_build_id']['#access'] = FALSE;
    $form['form_token']['#access'] = FALSE;
    $form['form_id']['#access'] = FALSE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function exposedFormElements() {
    return [];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function exposedFormSubmit(array &$form, FormStateInterface $form_state) {
    $this->exposed_data = $form_state->getValues();
  }

  /**
   * Return value by key or default value.
   *
   * @param string $key
   * @param bool $default
   * @return bool|mixed
   */
  protected function getExposedData($key, $default = FALSE) {
    $isset = isset($this->exposed_data[$key]);
    return $isset ? $this->exposed_data[$key] : $default;
  }

}
