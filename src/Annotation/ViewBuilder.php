<?php

namespace Drupal\view_builder\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object for view builder page.
 *
 * @Annotation
 */
class ViewBuilder extends Plugin {

  /**
   * A unique identifier for the view builder plugin.
   *
   * @var string
   */
  public $id;

  /**
   * A plugin type page or block.
   *
   * @var string
   */
  public $type;

  /**
   * A plugin title used page title.
   *
   * @var string
   */
  public $title;

  /**
   * A requirements view builder plugin.
   *
   * @var array
   */
  public $requirements;

  /**
   * The route path for plugin.
   *
   * @var string
   */
  public $path;

  /**
   * The template name for auto create.
   *
   * @var string
   */
  public $template;

}
