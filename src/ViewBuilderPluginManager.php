<?php

namespace Drupal\view_builder;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Factory\ContainerFactory;

class ViewBuilderPluginManager extends DefaultPluginManager {

  public function __construct($type, \Traversable $namespaces, ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $subdir = 'Plugin/ViewBuilder';
    $plugin_interface = ViewBuilderPluginInterface::class;
    $plugin_definition_annotation_name = 'Drupal\view_builder\Annotation\ViewBuilder';
    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
    $this->setCacheBackend($cache_backend, 'ViewBuilder');
    $this->factory = new ContainerFactory($this->getDiscovery());
  }

}
