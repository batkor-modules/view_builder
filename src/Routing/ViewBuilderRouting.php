<?php

namespace Drupal\view_builder\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\view_builder\ViewBuilderPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines a route to register a url for serving vuew builder page service.
 */
class ViewBuilderRouting implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The View builder plugin manager service.
   *
   * @var \Drupal\view_builder\ViewBuilderPluginManager
   */
  protected $plugin_service;

  public function __construct(ViewBuilderPluginManager $plugin_service) {
    $this->plugin_service = $plugin_service;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.view_builder.page')
    );
  }

  /**
   * Returns an array of route objects.
   *
   * @return RouteCollection
   *   An array of route objects.
   */
  public function routes() {
    $route_collection = new RouteCollection();

    foreach ($this->plugin_service->getDefinitions() as $definition) {
      if (isset($definition['type']) && $definition['type'] === 'page'){
        $defaults = [
          '_controller' => self::class. "::build",
          '_title_callback' => self::class. '::title',
          'plugin_id' => $definition['id'],
        ];
        $route = new Route(
          $definition['path'],
          $defaults,
          $definition['requirements']
        );
        $route_collection->add($definition['id'], $route);
      }
    }
    return $route_collection;
  }

  /**
   * Return page title.
   *
   * @param $plugin_id
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function title($plugin_id){
    return $this->getInstance($plugin_id)->getTitle();
  }

  /**
   * Render page content.
   *
   * @param $plugin_id
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function build(Request $request, $plugin_id) {
    $params = $request->attributes->get('_raw_variables')->all();
    return $this->getInstance($plugin_id)->setParameters($params)->renderer();
  }

  /**
   * Return plugin instance.
   *
   * @param $plugin_id
   * @return object|\Drupal\view_builder\ViewBuilderPluginInterface
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getInstance($plugin_id){
    return $this->plugin_service->createInstance($plugin_id);
  }

}
