<?php

namespace Drupal\view_builder_example\Plugin\ViewBuilder;

use Drupal\node\Entity\Node;
use Drupal\view_builder\Annotation\ViewBuilder;
use Drupal\view_builder\Plugin\ViewBuilder\ViewBuilderBase;

/**
 * Class Example.
 *
 * @ViewBuilder(
 *   id = "example_page",
 *   type = "page",
 *   path = "my_page/{node_type}/{id}",
 *   template = "example_template",
 *   requirements = {
 *    "_permission" = "access content",
 *    "node_type": "[a-zA-Z]+",
 *    "id": "[0-9]+"
 *   }
 * )
 */
class ExamplePage extends ViewBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return 'Exampe page';
  }

  /**
   * {@inheritdoc}
   */
  public function renderer() {
    $build = [];
    //$build[] = $this->renderExposedForm();
    $storage = $this->entityTypeManager->getStorage('node');
    $viewBuilder = $this->entityTypeManager->getViewBuilder('node');
    $query = $storage->getQuery();
    $query->condition('type', $this->getParameter('node_type'));
    if ($id = $this->getExposedData('id')) {
      $query->condition('nid', $id);
    }
    $result = $query->execute();
    /** @var Node $node */
    foreach ($storage->loadMultiple($result) as $node) {
      $content[] = $viewBuilder->view($node,'teaser');
    }
    $build[] = [
      '#theme' => 'example_template',
      '#content' => $content,
    ];
    return $build;
  }


  /**
   * {@inheritdoc}
   */
  public function exposedFormElements() {
    $form = [];
    $form['id'] = [
      '#title' => $this->t('ID'),
      '#type' => 'textfield',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#name' => '',
      '#value' => $this->t('Apply'),
      /**
       * @see ViewBuilderBase::exposedFormSubmit()
       */
      '#submit' => [[$this, 'exposedFormSubmit']],
      '#button_type' => 'primary',
    ];
    return $form;
  }

}
