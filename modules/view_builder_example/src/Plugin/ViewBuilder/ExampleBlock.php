<?php

namespace Drupal\view_builder_example\Plugin\ViewBuilder;

use Drupal\view_builder\Plugin\ViewBuilder\ViewBuilderBase;

/**
 * Class Example Block.
 *
 * @ViewBuilder(
 *   id = "example",
 *   title = "Example",
 *   type = "block",
 *   template = "example_template",
 * )
 */
class ExampleBlock extends ViewBuilderBase {

  /**
   * {@inheritdoc}
   */
  public function renderer() {
    return [
      '#markup' => __CLASS__,
    ];
  }

}